#!/usr/bin/perl

use strict;
use warnings;
if(-e "/home/student/errors.txt"){
        unlink("/home/student/errors.txt");
}
my @emergencies;
my @alerts;
my @criticals;
my @errors;
my @warnings;
my @notices;
my @infos;
my @debugs;
my @files=glob("/var/log/httpd/error*");
system('touch /home/student/errors.txt');
open(my $out,'>',"/home/student/errors.txt") or die $!;
foreach my $file(@files){
        open(my $in,'<',"$file") or die $!;
        print("opened $file\n");
        open(my $out,'>',"/home/student/errors.txt") or die $!;
        #system('touch /home/student/errors.txt')
        while(my $line=<$in>){
                chomp($line);
                #use regex to match keyword m/keyword/
                if($line=~m/emerg/){
                        push(@emergencies, $line);
                }elsif($line=~m/alert/){
                        push(@alerts, $line);
                }elsif($line=~m/crit/){
                        push(@criticals, $line);
                }elsif($line=~m/error/){
                        push(@errors, $line);
                }elsif($line=~m/warn/){
                        push(@warnings, $line);
                }elsif($line=~m/notice/){
                        push(@notices, $line);
                }elsif($line=~m/info/){
                        push(@infos, $line);
                }elsif($line=~m/debug/){
                        push(@debugs, $line);
                }
        }
}
        print($out "Emergencies"."\n----------------------------\n");
        if(@emergencies){
        foreach  my $elem(@emergencies){
                print($out $elem. "\n");}
        print($out "\n");}else{print($out "None found in log.\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");}
        print($out "Alerts"."\n----------------------------\n");
        if(@alerts){
        foreach  my $elem(@alerts){
                print($out $elem. "\n");}
        print($out "\n");}else{print($out "None found in log.\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");}
        print($out "Critical"."\n----------------------------\n");
        if(@criticals){
        foreach  my $elem(@criticals){
                print($out $elem. "\n");}
        print($out "\n");}else{print($out "None found in log.\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");}
        print($out "Errors"."\n----------------------------\n");
        if(@errors){
        foreach  my $elem(@errors){
                print($out $elem. "\n");}
        print($out "\n");}else{print($out "None found in log.\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");}
        print($out "Warnings"."\n----------------------------\n");
        if(@warnings){
        foreach  my $elem(@warnings){
                print($out $elem. "\n");}
       print($out "\n");}else{print($out "None found in log.\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");}
        print($out "Notices"."\n----------------------------\n");
        if(@notices){
        foreach  my $elem(@notices){
                print($out $elem. "\n");}
        print($out "\n");}else{print($out "None found in log.\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");}
        print($out "Informational"."\n----------------------------\n");
        if(@infos){
        foreach  my $elem(@infos){
                print($out $elem. "\n");}
       print($out "\n");}else{print($out "None found in log.\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");}
        print($out "Debug Messages"."\n----------------------------\n");
        if(@debugs){
        foreach  my $elem(@debugs){
                print($out $elem. "\n");}
       print($out "\n");}else{print($out "None found in log.\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");}

