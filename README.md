linux-stuff
===========

programs, scripts and code written to be compatible with a Linux or UNIX-like OS

vmware_patch was taken from http://vcojot.blogspot.com/2015/11/vmware-worksation-12-on-fedora-core-23.html with permission. Thanks and credit go to Mr. Cojot. bl-* is forked from githhub.com/bunsenlabs
wmtiler.sh was found here:
https://bbs.archlinux.org/viewtopic.php?id=119020

dmenu power script is from https://bbs.archlinux.org/viewtopic.php?id=95984 in order to work it must be placed in /usr/bin
